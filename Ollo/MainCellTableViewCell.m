//
//  MainCellTableViewCell.m
//  Ollo
//
//  Created by Allan.Chan on 9/16/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"
#import "MainCellTableViewCell.h"
#import "KeychainItemWrapper.h"
#import "UtilityHelper.h"
@implementation MainCellTableViewCell

- (void)awakeFromNib {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self.ruleSwitch addTarget:self action:@selector(turnOnRule) forControlEvents:UIControlEventValueChanged];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)setupCell:(RuleModel *)model
{
    self.ruleNameLab.text = model.ruleName;
    [self filterConditionAndAction:model];
    self.ruleSwitch.on = ![model.turnOrOff intValue];
}


- (void)filterConditionAndAction:(RuleModel *)model
{
    NSUserDefaults *ruleSampleInfo = [NSUserDefaults standardUserDefaults];
    DeviceModel *deviceModel = [[DeviceModel alloc] initDeviceModel];
    
    NSString *downloadConditionsIconsSting;
    NSString *downloadActionsIconsSting;
    for (int i=0; i<model.preconditions.count; i++ )
    {
        if (![[[model.preconditions objectAtIndex:i] objectForKey:@"handler"] isEqualToString:@"datetime"])
        {
            
            if ([[[model.preconditions objectAtIndex:i] objectForKey:@"params"] objectForKey:@"guid"])
            {
                //获取GUID Rule.json
                downloadConditionsIconsSting = [[[model.preconditions objectAtIndex:i] objectForKey:@"params"] objectForKey:@"guid"];
                
                //获取DeviceType Device.json
                [deviceModel initValue:downloadConditionsIconsSting];
                
                //获取IconsImages Trigger-Action.json
                downloadConditionsIconsSting = [[[ruleSampleInfo objectForKey:@"ruleSampleDic"] objectForKey:deviceModel.deviceType] objectForKey:@"config_icon"];
            }
            else
            {
                downloadConditionsIconsSting = [[NSString alloc] initWithFormat:@"%@%@",DOMAINNAME,@"/images/rule/devices/sensor/datetime-small.png"];
            }
            
        }
        else
        {
            downloadConditionsIconsSting = [[NSString alloc] initWithFormat:@"%@%@",DOMAINNAME,@"/images/rule/devices/sensor/datetime-small.png"];
        }
        
        UIImageView *conditionImageView = [[UIImageView alloc] initWithFrame:CGRectMake(27*i+6, 0, 27, 27)];
        [conditionImageView setImageWithURL:[NSURL URLWithString:downloadConditionsIconsSting] placeholderImage:[UIImage imageNamed:@"default.png"]];
        [self.preconditionView addSubview:conditionImageView];
    }
    
    
    for (int i=0; i<model.action.count; i++ )
    {
        if (![[[model.action objectAtIndex:i] objectForKey:@"handler"] isEqualToString:@"datetime"])
        {
            
            if ([[[model.action objectAtIndex:i] objectForKey:@"params"] objectForKey:@"guid"])
            {
                //获取GUID Rule.json
                downloadActionsIconsSting = [[[model.action objectAtIndex:i] objectForKey:@"params"] objectForKey:@"guid"];
                
                //获取DeviceType Device.json
                [deviceModel initValue:downloadActionsIconsSting];
                
                //获取IconsImages Trigger-Action.json
                downloadActionsIconsSting = [[NSString alloc] initWithFormat:@"%@%@",DOMAINNAME,[[[ruleSampleInfo objectForKey:@"ruleSampleDic"] objectForKey:deviceModel.deviceType] objectForKey:@"select_icon"]];
            }
            else
            {
                downloadConditionsIconsSting = [[NSString alloc] initWithFormat:@"%@%@",DOMAINNAME,@"/images/rule/devices/sensor/datetime-small.png"];
            }
            
        }
        else
        {
            downloadConditionsIconsSting = [[NSString alloc] initWithFormat:@"%@%@",DOMAINNAME,@"/images/rule/devices/sensor/datetime-small.png"];
        }
        UIImageView *actionImageView = [[UIImageView alloc] initWithFrame:CGRectMake(27*i+6, 0, 27, 27)];
        [actionImageView setImageWithURL:[NSURL URLWithString:downloadActionsIconsSting] placeholderImage:[UIImage imageNamed:@"default.png"]];
        [self.actionView addSubview:actionImageView];
    }
}


-(void)turnOnRule
{
    self.tableDataHelper = [[TableDataHelper alloc] init];
    [self.tableDataHelper setInterfaceDelegate:self];
    NSString *ruleONorOff = [[NSString alloc] initWithFormat:@"%@%@%@/%@",DOMAINNAME,APIVERSION,RULEURL,SUSPENDURL];
    
    KeychainItemWrapper *accountAccessToken = [[KeychainItemWrapper alloc] initWithIdentifier:@"AccountAccessToken" accessGroup:nil];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] initWithCapacity:10];
    [postDic setObject:[accountAccessToken objectForKey:(id)CFBridgingRelease(kSecAttrAccount)] forKey:@"access_token"];
    
    if (self.ruleSwitch.on) {
        [self.tableDataHelper requestByAsync:ruleONorOff andParameter:postDic andMethod:@"DELETE" andTag:0 andPassParameter:nil];
    }
    else
    {
        [self.tableDataHelper requestByAsync:ruleONorOff andParameter:postDic andMethod:@"POST" andTag:1 andPassParameter:nil];
    }

}

-(void)uploadDataReturnDic:(NSDictionary *)returnDic andTag:(NSInteger)requestTag andPassParameter:(NSMutableDictionary *)passParameter
{
    if (requestTag == 0)
    {
        self.ruleSwitch.on = false;
    }
    
    else
    {
        self.ruleSwitch.on = true;
    }
}

-(void)uploadDataFail:(NSDictionary *)returnDic andError:(NSError *)error andTag:(NSInteger)requestTag andPassParameter:(NSMutableDictionary *)passParameter
{
    [UtilityHelper showAlertView:@"##出错了##" andMessage:@"请检查您的网络" andCancelBtnTitle:@"确认"];
}

@end
