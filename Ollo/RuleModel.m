//
//  RuleModel.m
//  Ollo
//
//  Created by Allan.Chan on 9/11/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import "RuleModel.h"

@implementation RuleModel
@synthesize ruleId;
@synthesize ruleName;
@synthesize turnOrOff;
@synthesize preconditions;
@synthesize action;
- (id)initWithRuleName:(NSString *)inputRuleName
                ruleID:(NSString *)inputRuleId
            turnOrOff:(NSString *) inputTurnOrOff
        preconditions:(NSMutableArray *)inputPreconditions
           action:(NSMutableArray *)inputAction
{
    self = [super init];
    if (self) {
        self.ruleId         = inputRuleId;
        self.ruleName       = inputRuleName;
        self.turnOrOff      = inputTurnOrOff;
        self.preconditions  = inputPreconditions;
        self.action         = inputAction;
    }
    return self;
}

@end
