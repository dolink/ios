//
//  MainViewController.m
//  Ollo
//
//  Created by Allan.Chan on 9/10/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import "KeychainItemWrapper.h"
#import "MainViewController.h"
#import "UtilityHelper.h"
#import "MainCellTableViewCell.h"
#import "RuleModel.h"
#import "DeviceModel.h"
#import "CreateRuleViewController.h"
@interface MainViewController ()
@end

@implementation MainViewController

@synthesize ruleDataArray;

- (void)viewDidLoad {
    [self initView];
    [super viewDidLoad];
}

-(void)initView
{
    self.ruleDataArray = [[NSMutableArray alloc] initWithCapacity:10];
    self.tableDataHelper = [[TableDataHelper alloc] init];
    [self.tableDataHelper setInterfaceDelegate:self];
    self.view.backgroundColor = [UtilityHelper colorWithHexString:@"f5f5f5"];
    self.ruleTableView.backgroundColor = [UIColor whiteColor];
    [self downloadRuleData];
    
}

-(void)downloadRuleData
{
    KeychainItemWrapper *accountAccessToken = [[KeychainItemWrapper alloc] initWithIdentifier:@"AccountAccessToken" accessGroup:nil];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] initWithCapacity:10];
    [postDic setObject:[accountAccessToken objectForKey:(id)CFBridgingRelease(kSecAttrAccount)] forKey:@"access_token"];
    NSString *ruleDataURLString = [[NSString alloc] initWithFormat:@"%@%@%@",DOMAINNAME,APIVERSION,RULEURL];
    [self.tableDataHelper requestByAsync:ruleDataURLString andParameter:postDic andMethod:@"GET" andTag:0 andPassParameter:nil];
}

-(void)downloadDeviceData
{
    KeychainItemWrapper *accountAccessToken = [[KeychainItemWrapper alloc] initWithIdentifier:@"AccountAccessToken" accessGroup:nil];
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] initWithCapacity:10];
    [postDic setObject:[accountAccessToken objectForKey:(id)CFBridgingRelease(kSecAttrAccount)] forKey:@"access_token"];
    NSString *deviceDataURLString = [[NSString alloc] initWithFormat:@"%@%@%@",DOMAINNAME,APIVERSION,DEVICELISTURL];
    [self.tableDataHelper requestByAsync:deviceDataURLString andParameter:postDic andMethod:@"GET" andTag:1 andPassParameter:nil];
}


-(void)uploadDataReturnDic:(NSDictionary *)returnDic andTag:(NSInteger)requestTag andPassParameter:(NSMutableDictionary *)passParameter
{
    if (requestTag == 0)
    {
        if ([[returnDic objectForKey:@"result"] intValue]==1)
        {
            [self downloadDeviceData];
            if ([[returnDic objectForKey:@"data"] count] != 0)
            {
                for (int i=0; i<[[returnDic objectForKey:@"data"] count]; i++)
                {
                    RuleModel *ruleModel = [[RuleModel alloc]
                                            initWithRuleName:[[[returnDic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"shortName"]
                                            ruleID:[[[returnDic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"rid"]
                                            turnOrOff:[[[returnDic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"suspended"]
                                            preconditions:[[[returnDic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"preconditions"]
                                            action:[[[returnDic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"actions"]];
                    [self.ruleDataArray addObject:ruleModel];
                }
            }
            else
            {
                self.ruleTableView.hidden = YES;
                self.ruleDataArray = nil;
            }
        }
    }
    else
    {
        if ([[returnDic objectForKey:@"result"] intValue]==1)
        {
            NSUserDefaults *deviceInfo = [NSUserDefaults standardUserDefaults];
            [deviceInfo setObject:[[returnDic objectForKey:@"data"] allKeys] forKey:@"deviceAllKeys"];
            [deviceInfo setObject:[[returnDic objectForKey:@"data"] allObjects] forKey:@"deviceAllObjects"];
            [deviceInfo synchronize];
            [self.ruleTableView reloadData];
        }
    }
}

-(void)uploadDataFail:(NSDictionary *)returnDic andError:(NSError *)error andTag:(NSInteger)requestTag andPassParameter:(NSMutableDictionary *)passParameter
{
    [UtilityHelper showAlertView:@"##出错了##" andMessage:@"请检查您的网络请求" andCancelBtnTitle:@"确定"];
}

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.ruleDataArray count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *RuleCell = @"RuleCell";
    
    static BOOL isRegNib = NO;
    if (!isRegNib) {
        [tableView registerNib:[UINib nibWithNibName:@"MainCellTableViewCell" bundle:nil] forCellReuseIdentifier:RuleCell];
        isRegNib = YES;
    }
    MainCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:RuleCell];
    CGFloat width = fmod(indexPath.row,2);
    if (width?1:0)
        cell.backgroundColor = [UtilityHelper colorWithHexString:@"ebebeb"];
    else
        cell.backgroundColor = [UIColor clearColor];
    
    [cell setupCell:self.ruleDataArray[indexPath.row]];
    return cell;
}

- (IBAction)createRulePress:(id)sender
{
    CreateRuleViewController *createRuleVC = [[CreateRuleViewController alloc] initWithNibName:@"CreateRuleViewController" bundle:nil];
    [self.navigationController pushViewController:createRuleVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
