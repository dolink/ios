//
//  ConditionAndResultVctrl.m
//  Ollo
//
//  Created by Bosim on 14-9-25.
//  Copyright (c) 2014年 Allan.Chan. All rights reserved.
//

#import "ConditionAndResultVctrl.h"
#import "ConditionAndResultCell.h"
#import "TriggerAndActionModel.h"
#import "ConfigWebVctrl.h"

@interface ConditionAndResultVctrl ()
{
    TriggerAndActionModel *dataModel;
    BOOL                   isResult;
    NSMutableArray        *dataArray;
}

@property(nonatomic,retain)IBOutlet UITableView *tableView;

@end

@implementation ConditionAndResultVctrl

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil data:(TriggerAndActionModel*)data
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])
    {
        dataArray = [[NSMutableArray alloc]init];
        dataModel = data;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataModel.conditionsArray.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellWithIdentifier = @"ConditionAndResultCell";
    
    ConditionAndResultCell *cell = [tableView dequeueReusableCellWithIdentifier:CellWithIdentifier];
    
    if (cell == nil)
    {
        NSArray* nib = [[NSBundle mainBundle] loadNibNamed:CellWithIdentifier owner:nil options:nil];
        for (id currentObject in nib)
        {
            if ([currentObject isKindOfClass:[ConditionAndResultCell class]])
            {
                cell = currentObject;
                break;
            }
        }
    }
    
    NSDictionary *dic = [dataModel.conditionsArray objectAtIndex:indexPath.row];
    
    cell.condtionTextLable.text = [dic objectForKey:@"title"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ConfigWebVctrl *vCtrl = [[ConfigWebVctrl alloc]initWithNibName:@"ConfigWebVctrl" bundle:nil data:dataModel index:indexPath.row];

    [self presentViewController:vCtrl animated:YES completion:^{
        
    }];
}
- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)controller
{
    return YES;
}




@end
