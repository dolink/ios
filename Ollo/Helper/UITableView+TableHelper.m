//
//  UITableView+TableHelper.m
//  JueWeiShop
//
//  Created by Allan.Chan on 13-2-17.
//
//
#import "UITableView+TableHelper.h"
#import <QuartzCore/QuartzCore.h>
#define KrefreshView 12871
#define KrefreshLab 12872
#define KrefreshActiveLoading 12873
#define KrefreshImageView 12874
@implementation UIScrollView (UIScrollViewHelper)

/*
 |  加载更多
 */
-(void)loadMore:(UIButton *)loadMoreBtn andActive:(UIActivityIndicatorView *)loadMoreActivity
{
    
    UIView *loadMoreView = (UIView *)[self.superview viewWithTag:100001];
    if (loadMoreView)
    {
        [self resetLoadMorePosition];
    }
    else
    {
        loadMoreView = [[UIView alloc]initWithFrame:CGRectMake(0, self.contentSize.height, 320, 52.0f)];
        [loadMoreView setBackgroundColor:[UIColor clearColor]];
        [loadMoreView addSubview:loadMoreActivity];
        [loadMoreView addSubview:loadMoreBtn];
        [loadMoreView setTag:100001];
        [self addSubview:loadMoreView];
    }
    self.contentInset = UIEdgeInsetsMake(0, 0, REFRESH_HEADER_HEIGHT, 0);
}

/*
 |  加载更多完成后，loadview的位置要重新设置
 */
-(void)resetLoadMorePosition
{
    UIView *loadMoreView = (UIView *)[self.superview viewWithTag:100001];
    loadMoreView.frame = CGRectMake(0, self.contentSize.height, 320, 52.0f);
}

/*
 |  清除加载更多的view
 */
-(void)removeLoadMoreView
{
    UIView *loadMoreView = (UIView *)[self.superview viewWithTag:100001];
    [loadMoreView removeFromSuperview];
    self.contentInset = UIEdgeInsetsZero;
}


/*
 |  下拉刷新
 */
-(void)refreshIterfaceInit
{
    /*
     | 初始化下拉刷新view的各个组件
     */
    
    UIView *refreshView = (UIView *)[self.superview viewWithTag:KrefreshView];
    UILabel *refreshLab = (UILabel *)[self.superview viewWithTag:KrefreshLab];
    UIActivityIndicatorView *activityIndicatouView = (UIActivityIndicatorView *)[self.superview viewWithTag:KrefreshActiveLoading];
    UIImageView *refreshImageView = (UIImageView *)[self.superview viewWithTag:KrefreshImageView];
    UIImageView *logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.center.x*0.5+10, 10, 134, 14)];
    [logoImageView setImage:[UIImage imageNamed:@"文字.png"]];
    refreshView = [[UIView alloc]initWithFrame:CGRectMake(0, -REFRESH_HEADER_HEIGHT, 320, 81.0f)];
    refreshLab = [[UILabel alloc]initWithFrame:CGRectMake(self.center.x*0.5+25, 30, 320, 50)];
    refreshImageView = [[UIImageView alloc]initWithFrame:CGRectMake(self.center.x*0.5, 50, 12, 12)];
    activityIndicatouView = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(self.center.x*0.5, 45, 20, 20)];
    [refreshView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"refresh底图.png"]]];
    [refreshLab setBackgroundColor:[UIColor clearColor]];
    [refreshLab setText:@"下拉可以刷新.."];
    [refreshLab setFont:[UIFont fontWithName:@"Helvetica-Bold" size:16]];
    [refreshLab setTextColor:[UIColor grayColor]];
    [refreshLab setTag:KrefreshLab];
    [refreshImageView setImage:[UIImage imageNamed:@"grayArrow.png"]];
    [refreshImageView setTag:KrefreshImageView];
    [activityIndicatouView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [activityIndicatouView  setHidesWhenStopped:YES];
    [activityIndicatouView  setTag:KrefreshActiveLoading];
    [refreshView addSubview:refreshLab];
    [refreshView addSubview:activityIndicatouView];
    [refreshView addSubview:refreshImageView];
    [refreshView addSubview:logoImageView];
    [refreshView setTag:KrefreshView];
    self.contentInset = UIEdgeInsetsMake(0, 0, REFRESH_HEADER_HEIGHT, 0);
    [self addSubview:refreshView];
    
}

-(void)changeRefreshViewLoadingMessage:(CGFloat)dropHeight
{
    UILabel *refreshLab = (UILabel *)[self.superview viewWithTag:KrefreshLab];
    UIImageView *refreshImageView = (UIImageView *)[self.superview viewWithTag:KrefreshImageView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.3];
    
    if (dropHeight < -REFRESH_HEADER_HEIGHT){
        [refreshLab setText:@"松开即可更新.."];
        [refreshImageView layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    }
    
    else if (dropHeight > -REFRESH_HEADER_HEIGHT){
        [refreshLab setText: @"下拉即可刷新.."];
        [refreshImageView layer].transform = CATransform3DMakeRotation(M_PI*2, 0, 0, 1);
    }
    [UIView commitAnimations];
    //NSLog(@"%f",self.contentOffset.y);
}

-(void)changeRefreshViewLoadingMessageWithoutParameters
{
    //NSLog(@"%f",self.contentOffset.y);
    float tableDropHeight = self.contentOffset.y;
    UILabel *refreshLab = (UILabel *)[self.superview viewWithTag:KrefreshLab];
    UIImageView *refreshImageView = (UIImageView *)[self.superview viewWithTag:KrefreshImageView];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.3];
    
    if (tableDropHeight < -REFRESH_HEADER_HEIGHT){
        [refreshLab setText:@"松开即可更新.."];
        [refreshImageView layer].transform = CATransform3DMakeRotation(M_PI, 0, 0, 1);
    }
    
    else if (tableDropHeight > -REFRESH_HEADER_HEIGHT){
        [refreshLab setText: @"下拉即可刷新.."];
        [refreshImageView layer].transform = CATransform3DMakeRotation(M_PI*2, 0, 0, 1);
    }
    [UIView commitAnimations];

}



/*
 | refreshing
 */
-(void)refreshingData
{
    UIActivityIndicatorView *activityIndicatouView = (UIActivityIndicatorView *)[self.superview viewWithTag:KrefreshActiveLoading];
    UIImageView *refreshImageView = (UIImageView *)[self.superview viewWithTag:KrefreshImageView];
    UILabel *refreshLab = (UILabel *)[self.superview viewWithTag:KrefreshLab];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    self.contentInset = UIEdgeInsetsMake(REFRESH_HEADER_HEIGHT, 0, 0, 0);
    [activityIndicatouView startAnimating];
    [refreshImageView setHidden:YES];
    [refreshLab setText:@"正在下载数据.."];
    [UIView commitAnimations];
}

-(void)stopRefreshingData
{
    UIActivityIndicatorView *activityIndicatouView = (UIActivityIndicatorView *)[self.superview viewWithTag:KrefreshActiveLoading];
    UIImageView *refreshImageView = (UIImageView *)[self.superview viewWithTag:KrefreshImageView];
    self.contentInset = UIEdgeInsetsZero;
    [activityIndicatouView stopAnimating];
    [refreshImageView setHidden:NO];
}


@end
