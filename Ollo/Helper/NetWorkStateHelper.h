//
//  NetWorkStateHelper.h
//  Ollo
//
//  Created by Allan.Chan on 10/10/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetWorkStateHelper : NSObject
+(NSString *)networkState:(NSInteger)stateInteger;
@end
