//
//  UIViewController+Helper.h
//  Lsgo
//
//  Created by Allan.Chan on 8/27/14.
//  Copyright (c) 2014 Allan. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "LoadingView.h"
@interface UIViewController (Helper)
-(void)showLoading;
-(void)removeLoading;
@end
