//
//  CreateRuleViewController.m
//  Ollo
//
//  Created by Allan.Chan on 9/15/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import "CreateRuleViewController.h"
#import "SelectTriggerAndActionViewController.h"
#import "TriggerAndActionModel.h"
#import "AFNetworking.h"
#import "UIButton+AFNetworking.h"

@interface CreateRuleViewController ()

@end

@implementation CreateRuleViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(recvNotify:) name:@"conditions" object:nil];
}

-(void)recvNotify:(NSNotification*)notify
{
    NSDictionary *dic = [notify userInfo];
    TriggerAndActionModel *data = [dic objectForKey:@"conditions"];
    
    UIButton *btn = (UIButton*)[self.view viewWithTag:101];
    [btn setImageForState:UIControlStateNormal withURL:[NSURL URLWithString:[[NSString alloc] initWithFormat:@"%@%@",DOMAINNAME,data.select_icon]] placeholderImage:[UIImage imageNamed:@"default.png"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)back:(id)sender {
    [self.navigationController   popViewControllerAnimated:YES];
}

- (IBAction)savePress:(id)sender {
    
}

-(IBAction)selectConditionAndAction:(id)sender
{
    SelectTriggerAndActionViewController *selectTAAVC = [[SelectTriggerAndActionViewController alloc] initWithNibName:@"SelectTriggerAndActionViewController" bundle:nil selectType:NO];
    
    [self.navigationController pushViewController:selectTAAVC animated:YES];
}



@end
