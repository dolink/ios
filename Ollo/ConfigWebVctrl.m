//
//  ConfigWebVctrl.m
//  Ollo
//
//  Created by Bosim on 14-9-26.
//  Copyright (c) 2014年 Allan.Chan. All rights reserved.
//

#import "ConfigWebVctrl.h"
#import "TriggerAndActionModel.h"
#import "WebViewJavascriptBridge.h"

@interface ConfigWebVctrl ()<UIWebViewDelegate>
{
    NSString *urlString;
    TriggerAndActionModel *dataModel;
    int      configIndex;
    WebViewJavascriptBridge* _bridge;
}

@end

@implementation ConfigWebVctrl

@synthesize configWebView;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil data:(TriggerAndActionModel*)data index:(int)index
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])
    {
        configIndex = index;
        dataModel = data;
        NSString *urlStr = [[dataModel.conditionsArray objectAtIndex:index]objectForKey:@"config_url"];
        if (urlStr && urlStr.length > 0)
        {
            urlStr = [urlStr substringFromIndex:1];
        }
        
        NSString *uString = [[NSString alloc] initWithFormat:@"%@%@",DOMAINNAME,urlStr];
        urlString = uString;

    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [WebViewJavascriptBridge enableLogging];
    _bridge = [WebViewJavascriptBridge bridgeForWebView:configWebView handler:^(id data, WVJBResponseCallback responseCallback) {
        NSLog(@"ObjC received message from JS: %@", data);
        responseCallback(@"Response for message from ObjC");
    }];
    
    [configWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    int a = 0;
    a++;
    return YES;
}


-(IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(IBAction)confirm:(id)sender
{
    NSString *handler = [[dataModel.conditionsArray objectAtIndex:configIndex]objectForKey:@"handler"];
    NSString *method = [[dataModel.conditionsArray objectAtIndex:configIndex]objectForKey:@"method"];
    
    //NSString *device = [self dataTOjsonString:dataModel.device];
    
    //NSString *js = [NSString stringWithFormat:@"OLLO.buildRule(\"%@\",\"%@\",\"%@\")",device,method,handler];

    NSString *js = [NSString stringWithFormat:@"OLLO.buildRule(\"%@\",\"%@\",\"%@\")",@"devices",method,handler];
    
    [_bridge send:js responseCallback:^(id response) {
        NSLog(@"sendMessage got response: %@", response);
    }];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"conditions" object:nil userInfo:[NSDictionary dictionaryWithObjectsAndKeys:dataModel, @"conditions",nil]];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

-(NSString*)dataTOjsonString:(id)object
{
    NSString *jsonString = nil;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:&error];
    
    if (jsonData)
    {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        jsonString = [jsonString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];  //去除掉首尾的空白字符和换行字符
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        jsonString = [jsonString stringByReplacingOccurrencesOfString:@" " withString:@""];
    }

    return jsonString;
}

@end
