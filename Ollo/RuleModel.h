//
//  RuleModel.h
//  Ollo
//
//  Created by Allan.Chan on 9/11/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RuleModel : NSObject
@property(nonatomic,retain) NSString *ruleId;
@property(nonatomic,retain) NSString *ruleName;
@property(nonatomic,retain) NSString *turnOrOff;
@property(nonatomic,retain)NSMutableArray *preconditions;
@property(nonatomic,retain)NSMutableArray *action;
- (id)initWithRuleName:(NSString *)inputRuleName
                ruleID:(NSString *)inputRuleId
             turnOrOff:(NSString *) inputTurnOrOff
         preconditions:(NSMutableArray *)inputPreconditions
                action:(NSMutableArray *)inputAction;
@end
