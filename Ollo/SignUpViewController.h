//
//  SignUpViewController.h
//  Ollo
//
//  Created by Allan.Chan on 9/11/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableDataHelper.h"
@protocol SingUpDelegate<NSObject>

-(void)fillUsernameAndPassword:(NSString *)username;

@end

@interface SignUpViewController : UIViewController<UITextFieldDelegate,InterfaceDelegate,UIGestureRecognizerDelegate>
@property (nonatomic,retain) TableDataHelper *tableDataHelper;
@property (nonatomic,retain) id <SingUpDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *emailTxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxt;
@property (weak, nonatomic) IBOutlet UITextField *rePasswordTxt;
@property (weak, nonatomic) IBOutlet UITextField *usernameTxt;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@end
