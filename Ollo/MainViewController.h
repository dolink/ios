//
//  MainViewController.h
//  Ollo
//
//  Created by Allan.Chan on 9/10/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableDataHelper.h"
#import "InterfaceDelegate.h"
#import "MainCellTableViewCell.h"

@interface MainViewController : UIViewController<InterfaceDelegate,UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *ruleTableView;
@property (nonatomic,retain) TableDataHelper *tableDataHelper;
@property (nonatomic,retain) NSMutableArray *ruleDataArray;
@end
