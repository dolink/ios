//
//  SignUpViewController.m
//  Ollo
//
//  Created by Allan.Chan on 9/11/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//
#import "UtilityHelper.h"
#import "ValidateHelper.h"
#import "SignUpViewController.h"

@interface SignUpViewController ()

@end

@implementation SignUpViewController
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)initView
{
    UITapGestureRecognizer *hidenKeyBoardTap = [[UITapGestureRecognizer alloc] init];
    [hidenKeyBoardTap setNumberOfTapsRequired:1];
    [hidenKeyBoardTap setNumberOfTouchesRequired:1];
    [hidenKeyBoardTap addTarget:self action:@selector(hidenKeyBoardAction)];
    [self.view addGestureRecognizer:hidenKeyBoardTap];
    
    self.backBtn.titleLabel.textColor = [UtilityHelper colorWithHexString:@"28cacc"];
    [self.backBtn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    self.tableDataHelper = [[TableDataHelper alloc] init];
    [self.tableDataHelper setInterfaceDelegate:self];
}

-(IBAction)signUpPress:(id)sender
{
    if ([self validateData])
    {
        NSMutableDictionary *postDic = [[NSMutableDictionary alloc] initWithCapacity:10];
        [postDic setObject:self.usernameTxt.text forKey:@"username"];
        [postDic setObject:self.emailTxt.text forKey:@"email"];
        [postDic setObject:self.passwordTxt.text forKey:@"password"];
        NSString *signupURLString = [[NSString alloc] initWithFormat:@"%@%@%@",DOMAINNAME,APIVERSION,SIGNUPURL];
        [self.tableDataHelper requestByAsync:signupURLString andParameter:postDic andMethod:@"POST" andTag:0 andPassParameter:nil];
    }
}

-(void)uploadDataReturnDic:(NSDictionary *)returnDic andTag:(NSInteger)requestTag andPassParameter:(NSMutableDictionary *)passParameter
{
    if ([[returnDic objectForKey:@"result"] intValue] == 1)
    {
        
    }
}

-(void)uploadDataFail:(NSDictionary *)returnDic andError:(NSError *)error andTag:(NSInteger)requestTag andPassParameter:(NSMutableDictionary *)passParameter
{
    
}

-(BOOL)validateData
{
    if (self.usernameTxt.text.length <1) {
        [UtilityHelper showAlertView:@"##出错了##" andMessage:@"请填写用户名" andCancelBtnTitle:@"确定"];
        return false;
    }
    
    if (![ValidateHelper isValidateEmail:self.emailTxt.text]) {
        [UtilityHelper showAlertView:@"##出错了##" andMessage:@"请输入正确的邮件" andCancelBtnTitle:@"确定"];
        return false;
    }
    
    /*
     if (![self.passwordTxt.text isEqualToString:self.rePasswordTxt.text])
     {
     [UtilityHelper showAlertView:@"##出错了##" andMessage:@"你的确认密码不正确" andCancelBtnTitle:@"确定"];
     return false;
     }
     */
    
    if (!self.passwordTxt.text.length >=6) {
        [UtilityHelper showAlertView:@"##出错了##" andMessage:@"请输入6位以上的密码" andCancelBtnTitle:@"确定"];
        return  false;
    }
    
    else
    {
        return YES;
    }
}

-(void)hidenKeyBoardAction
{
    [self.usernameTxt resignFirstResponder];
    [self.emailTxt resignFirstResponder];
    [self.passwordTxt resignFirstResponder];
}

-(void)back
{
    [self dismissViewControllerAnimated:YES completion:^{
        [self.delegate fillUsernameAndPassword:self.emailTxt.text];
    }];
}
@end
