//
//  main.m
//  Ollo
//
//  Created by Allan.Chan on 9/10/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
