//
//  LoginViewController.m
//  Ollo
//
//  Created by Allan.Chan on 9/10/14.
//  Copyright (c) 2014 Allan.Chan. All rights reserved.
//

#import <pop/POP.h>
#import "UtilityHelper.h"
#import "LoginViewController.h"
#import "SignUpViewController.h"
#import "KeychainItemWrapper.h"
#import "MainViewController.h"
#import "UIViewController+Helper.h"
#import "NetWorkStateHelper.h"
@interface LoginViewController ()<SingUpDelegate>

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
}

-(void)initView
{
    self.tableDataHelper = [[TableDataHelper alloc] init];
    [self.tableDataHelper setInterfaceDelegate:self];
    [self.usernameTxt becomeFirstResponder];
    self.signupBtn.titleLabel.textColor = [UtilityHelper colorWithHexString:@"28cacc"];
    
    UITapGestureRecognizer *hidenKeyBoardTap = [[UITapGestureRecognizer alloc] init];
    [hidenKeyBoardTap setNumberOfTapsRequired:1];
    [hidenKeyBoardTap setNumberOfTouchesRequired:1];
    [hidenKeyBoardTap addTarget:self action:@selector(hidenKeyBoardAction)];
    [self.view addGestureRecognizer:hidenKeyBoardTap];
}

-(IBAction)userManagerPress:(id)sender
{
    [self.tableDataHelper requestByAsync:@"" andParameter:nil andMethod:@"POST" andTag:3 andPassParameter:nil];
}

-(IBAction)signupPress:(id)sender
{
    SignUpViewController *signUpVC = [[SignUpViewController alloc] initWithNibName:@"SignUpViewController" bundle:nil];
    [signUpVC setDelegate:self];
    [self presentViewController:signUpVC animated:YES completion:^{
    }];
}

-(IBAction)loginPress:(id)sender
{
    if ([self loginAction])
    {
        NSMutableDictionary *postDic = [[NSMutableDictionary alloc] initWithCapacity:10];
        [postDic setObject:self.usernameTxt.text forKey:@"username"];
        [postDic setObject:self.passwordTxt.text forKey:@"password"];
        NSString *loginURLString = [[NSString alloc] initWithFormat:@"%@%@%@",DOMAINNAME,APIVERSION,LOGINURL];
        [self.tableDataHelper requestByAsync:loginURLString andParameter:postDic andMethod:@"POST" andTag:0 andPassParameter:nil];
        [self showLoading];
    }
}

-(void)uploadDataReturnDic:(NSDictionary *)returnDic andTag:(NSInteger)requestTag andPassParameter:(NSMutableDictionary *)passParameter
{
    if (requestTag == 1)
    {
        [self removeLoading];
        if ([[returnDic objectForKey:@"result"] intValue] == 1)
        {
            NSMutableDictionary *ruleSampleDic = [[NSMutableDictionary alloc] initWithCapacity:10];
            for (int i = 0; i<[[returnDic objectForKey:@"data"] count]; i++)
            {
                if ([[[[returnDic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"filter"] objectForKey:@"DeviceType"])
                {
                    [ruleSampleDic setObject:[[returnDic objectForKey:@"data"] objectAtIndex:i] forKey:[[[[returnDic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"filter"] objectForKey:@"DeviceType"]];
                }
                
                else if ([[[[returnDic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"filter"] objectForKey:@"DID"])
                {
                     [ruleSampleDic setObject:[[returnDic objectForKey:@"data"] objectAtIndex:i] forKey:[[[[returnDic objectForKey:@"data"] objectAtIndex:i] objectForKey:@"filter"] objectForKey:@"DID"]];
                }

            }
            
            NSUserDefaults *ruleSampleInfo = [NSUserDefaults standardUserDefaults];
            [ruleSampleInfo setObject:ruleSampleDic forKey:@"ruleSampleDic"];
            [ruleSampleInfo synchronize];
            
            MainViewController *mainVC = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
            [self.navigationController pushViewController:mainVC animated:YES];
        }
    }
    else
    {
        if ([[returnDic objectForKey:@"result"] intValue]==1)
        {
            KeychainItemWrapper *accountAccessToken = [[KeychainItemWrapper alloc] initWithIdentifier:@"AccountAccessToken" accessGroup:nil];
            [accountAccessToken setObject:CFBridgingRelease(kSecAttrAccessibleWhenUnlocked) forKey:CFBridgingRelease(kSecAttrAccessible)];
            [accountAccessToken setObject:[[returnDic objectForKey:@"data"]objectForKey:@"id"] forKey:(id)CFBridgingRelease(kSecAttrAccount)];
            [accountAccessToken setObject:[[returnDic objectForKey:@"data"] objectForKey:@"ttl"] forKey:(id)CFBridgingRelease(kSecAttrService)];
            NSString *showTriggerAndActionJsonString = [[NSString alloc] initWithFormat:@"%@%@",DOMAINNAME,TRIGGERANDACTIONURL];
            [self.tableDataHelper requestByAsync:showTriggerAndActionJsonString andParameter:nil andMethod:@"GET" andTag:1 andPassParameter:nil];
        }
        else
        {
            [UtilityHelper showAlertView:@"##出错了##" andMessage:@"用户名或密码错误" andCancelBtnTitle:@"确认"];
        }
    }
}
-(void)uploadDataFail:(NSDictionary *)returnDic andError:(NSError *)error andTag:(NSInteger)requestTag andPassParameter:(NSMutableDictionary *)passParameter
{
    [self removeLoading];
    if (![returnDic objectForKey:@"error"])
    {
        [UtilityHelper showAlertView:@"##出错了##" andMessage:@"网络错误，请稍后再试" andCancelBtnTitle:@"确定"];
    }
    else
    {
        NSString *errorMsg = [NetWorkStateHelper networkState:[[[returnDic objectForKey:@"error"] objectForKey:@"status"] integerValue]];
        [UtilityHelper showAlertView:@"##出错了##" andMessage:errorMsg andCancelBtnTitle:@"确定"];
    }

}

-(void)fillUsernameAndPassword:(NSString *)username
{
    self.usernameTxt.text = username;
}

-(BOOL)loginAction
{
    if (self.usernameTxt.text.length == 0  || self.passwordTxt.text.length == 0)
    {
        [UtilityHelper showAlertView:@"##出错了##" andMessage:@"请填写用户名或密码" andCancelBtnTitle:@"确认"];
        return false;
    }
    
    if (self.passwordTxt.text.length < 6) {
        [UtilityHelper showAlertView:@"##出错了##" andMessage:@"请填写正确的密码" andCancelBtnTitle:@"确定"];
        return false;
    }
    else
    {
        return true;
    }
}

-(void)hidenKeyBoardAction
{
    [self.usernameTxt resignFirstResponder];
    [self.passwordTxt resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
